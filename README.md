# Frontend Playground
This is a playground blending lots of tools for easier functional View of MVC architectural style development to test my several ideas. Like any other playground, the ultimate purpose of this project is to maturize ideas which will be finally graduated and refactored in a library of its own gitlab path and published to Scaladex for import in my other real life application.
# Main Ideas
- This project differs from Udash by separating frontend from backend concerns.
