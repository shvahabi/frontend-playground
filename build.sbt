enablePlugins(ScalaJSPlugin)

name := "Frontend Playground"
scalaVersion := "2.13.3" // or any other Scala version >= 2.11.12

// This is an application without a main method
scalaJSUseMainModuleInitializer := false

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "1.0.0"
jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv()

libraryDependencies += "com.lihaoyi" %%% "utest" % "0.7.4" % "test"
testFrameworks += new TestFramework("utest.runner.Framework")
