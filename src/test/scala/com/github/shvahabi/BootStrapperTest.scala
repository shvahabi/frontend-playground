package com.github.shvahabi

import utest._

import scala.scalajs.js

import org.scalajs.dom
import org.scalajs.dom.document
import org.scalajs.dom.ext._

object MyNewScalaJSAppTest extends TestSuite {

  MyNewScalaJSApp.greetings()

  def tests = Tests {
    test("HelloWorld") {
      assert(document.querySelectorAll("p").count(_.textContent == "Hello world!") == 1)
    }
  }
}
